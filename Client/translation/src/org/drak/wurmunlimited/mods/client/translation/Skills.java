package org.drak.wurmunlimited.mods.client.translation;

import com.wurmonline.client.game.SkillLogic;
import com.wurmonline.client.game.SkillLogicSet;
import com.wurmonline.client.renderer.gui.HealthBar;
import com.wurmonline.client.renderer.gui.MindLogicCalculator;
import com.wurmonline.client.renderer.gui.SkillTrackerInfo;
import com.wurmonline.client.settings.PlayerData;
import org.drak.wurmunlimited.mods.client.translation.service.Vocabulary;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.drak.wurmunlimited.mods.client.translation.Translation.*;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.callPrivateMethod;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.getMethod;

/**
 * Created by Drak on 27.10.2018.
 */
public class Skills implements WurmClientMod {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());
    private boolean alreadyInit = false;

    public Skills() {
        try {
            skills();
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    //TODO: skills
    void skills() throws Exception {
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.SkillWindowComponent$SkillTreeListItem", "getHoverStrings", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            String EFFECTIVE = getTranslate("Effective", translated.gui);
                            String CHANGE = getTranslate("Change", translated.gui);
                            String HIGHEST = getTranslate("Highest", translated.gui);
                            String AFFINITIES = getTranslate("Affinities", translated.gui);
                            List<String> hoverStrings = (List<String>) args[0];
                            SkillLogic skill = (SkillLogic) args[1];
                            DecimalFormat longDecimalFormat = new DecimalFormat("##0.000000");
                            String skillName = getTranslate(skill.getName(), translated.skills);
                            boolean belowMax = skill.getValue() < skill.getMaxValue();
                            float delta = skill.getTotalValueChange();
                            hoverStrings.clear();
                            if(skill.getValue() == 0.0F) {
                                hoverStrings.add(skillName);
                            } else {
                                hoverStrings.add(skillName + ": " + longDecimalFormat.format((double)skill.getValue()));
                                if(skill.hasEffectiveValue()) {
                                    hoverStrings.add(EFFECTIVE + ": " + longDecimalFormat.format((double)skill.getEffectiveValue()));
                                }
                            }

                            if(delta != 0.0F) {
                                hoverStrings.add(CHANGE + ": " + longDecimalFormat.format((double)delta));
                            }

                            if(belowMax) {
                                hoverStrings.add(HIGHEST + ": " + longDecimalFormat.format((double)skill.getMaxValue()));
                            }

                            if(skill.getAffinities() > 0) {
                                hoverStrings.add(AFFINITIES + ": " + skill.getAffinities());
                            }
                            result = this;
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.SkillWindowComponent$SkillTreeListItem", "getName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Vocabulary[] vocabularies = new Vocabulary[]{translated.skills};
                            String name = (String) method.invoke(object, args);
                            // Ropemaking [3x] | Healing *
                            Pattern pattern = Pattern.compile("^(.+)( \\[.+\\]| \\*)$", Pattern.UNICODE_CHARACTER_CLASS);
                            Matcher matcher = pattern.matcher(name);
                            if (matcher.matches()) {
                                name = getTranslate(matcher.group(1), translated.skills) + matcher.group(2);
                            } else {
                                name = getTranslate(name, vocabularies);
                            }
                            result = capitalizeFirstLetter(name);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

    //todo Skill tracker
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.SkillTracker", "getSkillText", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Vocabulary[] vocabularies = new Vocabulary[]{translated.skills};
                            SkillTrackerInfo skill = (SkillTrackerInfo) args[0];
                            String skillName = getTranslate(skill.getName(), vocabularies);
                            skillName = capitalizeFirstLetter(skillName);
                            switch (skill.getDecimalsOptions()) {
                                case 0:
                                    return skillName + " " + String.format("%.6f", new Object[]{Float.valueOf(skill.getValue())}) + " (" + String.format("%.6f", new Object[]{Float.valueOf(skill.getChangedValue())}) + ") ";
                                case 1:
                                    return skillName + " " + String.format("%.0f", new Object[]{Float.valueOf(skill.getValue())}) + " (" + String.format("%.0f", new Object[]{Float.valueOf(skill.getChangedValue())}) + ") ";
                                case 2:
                                    return skillName + " " + String.format("%.1f", new Object[]{Float.valueOf(skill.getValue())}) + " (" + String.format("%.1f", new Object[]{Float.valueOf(skill.getChangedValue())}) + ") ";
                                case 3:
                                    return skillName + " " + String.format("%.2f", new Object[]{Float.valueOf(skill.getValue())}) + " (" + String.format("%.2f", new Object[]{Float.valueOf(skill.getChangedValue())}) + ") ";
                                case 4:
                                    return skillName + " " + String.format("%.3f", new Object[]{Float.valueOf(skill.getValue())}) + " (" + String.format("%.3f", new Object[]{Float.valueOf(skill.getChangedValue())}) + ") ";
                                default:
                                    return skillName + " " + skill.getValue() + " (" + skill.getChangedValue() + ") ";
                            }
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.SkillTrackerOptionsWindow", "updateSkillList", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            String[] allSkills = (String[]) args[0];
                            for (int i = 0; i < allSkills.length; i++) {
//                                logger.log(Level.WARNING, "skill>>" + allSkills[i]);
                                allSkills[i] = capitalizeFirstLetter(getTranslate(allSkills[i], translated.skills));
                            }
                            args[0] = allSkills;
                            result = method.invoke(object, args);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.SkillTrackerOptionsWindow", "updateSkillList", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            String[] allSkills = (String[]) args[0];
                            for (int i = 0; i < allSkills.length; i++) {
                                allSkills[i] = capitalizeFirstLetter(getTranslate(allSkills[i], translated.skills));
                            }
                            args[0] = allSkills;
                            result = method.invoke(object, args);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.SkillTrackerOptionsWindow$Entity", "getValueFromList", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            String skillName = (String) args[0];
                            String[] allSkills = (String[]) args[1];
                            skillName = getTranslate(skillName, translated.skills);
                            for (int i = 0; i < allSkills.length; ++i) {
                                if (skillName.equalsIgnoreCase(allSkills[i])) {
                                    return i;
                                }
                            }
                            return 0;

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

    }
}

