package org.drak.wurmunlimited.mods.client.translation.service;

import org.drak.wurmunlimited.mods.client.translation.Translation;

import java.util.logging.Logger;

/**
 * Created by Drak on 07.09.2018.
 */
public class Vocabularies {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());
    public Vocabulary items;
    public Vocabulary creatures;
    public Vocabulary materials;
    public Vocabulary roots;
    public Vocabulary prefixes;
    public Vocabulary tiles;
    public Vocabulary gui;
    public Vocabulary skills;

    public Vocabularies(String language) {
        items = new Vocabulary("items.xml", language);
        creatures = new Vocabulary("creatures.xml", language);
        materials = new Vocabulary("materials.xml", language);
        roots = new Vocabulary("roots.xml", language);
        prefixes = new Vocabulary("prefixes.xml", language);
        tiles = new Vocabulary("tiles.xml", language);
        gui = new Vocabulary("interface.xml", language);
        skills = new Vocabulary("skills.xml", language);

        items.loadFile();
        creatures.loadFile();
        materials.loadFile();
        roots.loadFile();
        prefixes.loadFile();
        tiles.loadFile();
        gui.loadFile();
        skills.loadFile();
    }
}
