package org.drak.wurmunlimited.mods.client.translation;

import com.wurmonline.client.renderer.PickData;
import com.wurmonline.client.renderer.gui.Recipe;
import com.wurmonline.client.renderer.gui.RecipeIngredientGroup;
import com.wurmonline.client.renderer.gui.RecipeListItem;
import com.wurmonline.client.renderer.gui.RecipeListWindow;
import org.drak.wurmunlimited.mods.client.translation.service.Vocabulary;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.lang.reflect.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.drak.wurmunlimited.mods.client.translation.Translation.*;
import static org.drak.wurmunlimited.mods.client.translation.Translation.getReverseTranslate;
import static org.drak.wurmunlimited.mods.client.translation.Translation.getTranslate;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.*;

/**
 * Created by Drak on 23.10.2018.
 */
public class Cooking implements WurmClientMod {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());
    private boolean alreadyInit = false;

    public Cooking() {
        try {
            cooking();
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    //TODO: gui
    void cooking() throws Exception {
        // Translates tree items in list.
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.WurmTreeList", "addTreeListItem", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Object item = args[0];
                            if (item.getClass() == RecipeListItem.class) {
                                short recipeId = (short) getFieldValue(item, "recipeId");
                                RecipeListWindow recipeListWindow = (RecipeListWindow) getFieldValue(item, "recipeListWindow");
                                Vocabulary[] vocabularies = new Vocabulary[]{translated.skills, translated.gui, translated.items};
                                String name = (String) getFieldValue(item, "name");
                                // regexp for hovers like "Skill: cooking"
                                Pattern pattern = Pattern.compile("^([\\W\\w\\s]+): ([\\w\\W\\s]+)$", Pattern.UNICODE_CHARACTER_CLASS);
                                Matcher matcher = pattern.matcher(name);
                                if (matcher.matches()) {
                                    name = getTranslate(matcher.group(1), translated.gui) + ": " + getItemTranslate(matcher.group(2), vocabularies);
                                } else {
                                    recipeId = (short) getFieldValue(item, "recipeId");
                                    Recipe recipe = recipeListWindow.getRecipe(recipeId);
                                    // Check if recipe not exist, for recipeId = -1 for example.
                                    if (recipe == null) {
                                        name = getItemTranslate(name, vocabularies);
                                    } else {
//                                        String recipeName = (String) getFieldValue(recipe, "name");
//                                        if (name.equals("paprika")) {
//                                            //logger.log(Level.WARNING, name + " <<<>>> " + recipeName + " ---" + recipeId);
//                                        }
//                                        // Translate untranslated recipes only.
//                                        if (recipeName.equals(name)) {
//                                            name = getItemTranslate(name, translated.items);
//                                        }
                                        // Looks like a bug, so we fix it by getting a name from a recipe.
                                        name = callPrivateMethod(recipe, getMethod(recipe.getClass(), "getName"));
                                    }
                                }
                                setFieldValue(item, "name", name);
                                args[0] = item;

                            }
                            result = method.invoke(object, args);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.Recipe", "getName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Vocabulary[] vocabularies = new Vocabulary[]{translated.items};
                            String name = (String) method.invoke(object, args);
                            name = getItemTranslate(name, vocabularies);
                            result = name;
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        // Translates over tooltips.
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.RecipeListItem", "getHoverDescription", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Vocabulary[] vocabularies = new Vocabulary[]{translated.gui};
                            PickData pd = (PickData) args[0];
                            //RecipeListWindow recipeListWindow = (RecipeListWindow)getFieldValue(object, "recipeListWindow");
                            String name = (String) getFieldValue(object, "name");
                            String notes = (String) getFieldValue(object, "notes");
                            short recipeId = (short) getFieldValue(object, "recipeId");
                            boolean isUnknown = (boolean) getFieldValue(object, "isUnknown");
                            boolean isGloballyKnown = (boolean) getFieldValue(object, "isGloballyKnown");

                            String textSkill = getTranslate("Skill", vocabularies);
                            String textActive = getTranslate("Active", vocabularies);
                            String textTool = getTranslate("Tool", vocabularies);
                            String textTarget = getTranslate("Target", vocabularies);

                            pd.addText(name + ".");
                            if (recipeId != -1) {
                                if (isUnknown) {
                                    pd.addText(getTranslate("You do not know this recipe!", vocabularies));
                                }

                                if (isGloballyKnown) {
                                    pd.addText(getTranslate("This recipe is known to everyone.", vocabularies));
                                }

                                if (notes.length() > 0) {
                                    pd.addText(notes);
                                }
                                //pd.addDevText("(RecipeId:" + this.recipeId + ")", this.recipeListWindow.isDev());
                            } else if (name.startsWith(textSkill + ": ")) {
                                String skill = name.substring((textSkill + ": ").length());
                                String reverseSkill = getReverseTranslate(skill, translated.skills);
                                float skillLevel = hud.getWorld().getPlayer().getSkillSet().getSkillValue(reverseSkill);
                                pd.addText(skill + ": " + skillLevel);
                            } else if (name.startsWith(textActive + ":")) {
                                if (name.equalsIgnoreCase(textActive + ": " + getTranslate("hand", vocabularies))) {
                                    pd.addText(getTranslate("You can use any active item for this recipe.", vocabularies));
                                } else {
                                    pd.addText(getTranslate("You need to activate this item to be able to make this recipe.", vocabularies));
                                }
                            } else if (!name.startsWith(textTool + ":") && !name.startsWith(textTarget + ":")) {
                                if (name.equalsIgnoreCase(getTranslate("Cookers", vocabularies))) {
                                    if (!isGloballyKnown) {
                                        pd.addText(getTranslate("This list of cookers may not include all the cookers that can be used to make this recipe.", vocabularies));
                                    }
                                } else if (name.equalsIgnoreCase(getTranslate("Containers", vocabularies))) {
                                    if (!isGloballyKnown) {
                                        pd.addText(getTranslate("This list of containers may not include all the containers that can be used to make this recipe.", vocabularies));
                                    }
                                } else if (name.equalsIgnoreCase(getTranslate("Mandatory", vocabularies))) {
                                    pd.addText(getTranslate("The following ingredients MUST be used in this recipe.", vocabularies));
                                } else if (name.equalsIgnoreCase(getTranslate("Zero or one", vocabularies))) {
                                    pd.addText(getTranslate("You may use a maximum of one of these ingredients.", vocabularies));
                                } else if (name.equalsIgnoreCase(getTranslate("One of", vocabularies))) {
                                    pd.addText(getTranslate("You MUST use one of the following ingredients.", vocabularies));
                                } else if (name.equalsIgnoreCase(getTranslate("One or more", vocabularies))) {
                                    pd.addText(getTranslate("You MUST use at least one of the following ingredients.", vocabularies));
                                } else if (name.equalsIgnoreCase(getTranslate("Optional", vocabularies))) {
                                    pd.addText(getTranslate("You may optionally use any of the following ingredients.", vocabularies));
                                } else if (name.equalsIgnoreCase(getTranslate("Any", vocabularies))) {
                                    pd.addText(getTranslate("You can use any of the following ingredients.", vocabularies));
                                }
                            }

                            result = this;
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
    }
}

