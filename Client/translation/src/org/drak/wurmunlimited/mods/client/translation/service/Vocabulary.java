package org.drak.wurmunlimited.mods.client.translation.service;

import org.drak.wurmunlimited.mods.client.translation.Translation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.drak.wurmunlimited.mods.client.translation.Translation.*;

/**
 * Created by Drak on 07.09.2018.
 */
public class Vocabulary {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());
    private String language;
    private String filename;
    public String filePath;
    public Map<String, String> dic = new HashMap<String, String>();
    public String[] tags = new String[]{"orig", "trans"};

    public Vocabulary(String filename, String language) {
        this.filename = filename;
        this.language = language;
        filePath = getJarDir() + "\\" + language + "\\" + filename;
    }

    public String getLanguage() {
        return language;
    }

    public String getFilename() {
        return filename;
    }

    public Map<String, String> getDic() {
        return dic;
    }

    public void loadFile() {

        int counter = 0;
        File xmlFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        String FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";
        try {
            dbFactory.setFeature(FEATURE, true);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            //System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
            NodeList nodeList = doc.getElementsByTagName("Row");
            counter = 0;
            for (int i = 0; i < nodeList.getLength(); i++) {
                TranslateUnit translate = setTranslate(nodeList.item(i));
                if (translate != null) {
                    //logger.log(Level.INFO, "Loaded " + translated.getOriginal());
                    String original = translate.getOriginal();
                    String translated = translate.getTranslated();
                    if (original != null && translated != null) {
                        counter += 1;
                        dic.put(translate.getOriginal().toLowerCase(), translate.getTranslated());
                        //logger.log(Level.INFO, translated.getOriginal() + " " + translated.getTranslated());
                    } else {
                        logger.log(Level.WARNING, "Found null value in loaded row.");
                    }
                } else {
                    //logger.log(Level.INFO, "skipped");
                }
            }

        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
        outLoadResult(counter, filename);
    }

    public void outLoadResult(int counter, String filename) {
        logger.log(Level.INFO, "Loaded " + counter + " translated rows from \'" + filename + "\'");
    }

    public TranslateUnit setTranslate(Node node) {
        TranslateUnit translate = new TranslateUnit();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            translate.setOriginal(getTagValue(tags[0], element));
            translate.setTranslated(getTagValue(tags[1], element));
        }
        return translate;
    }

    private String getTagValue(String tag, Element element) {
        try {
            NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
            Node node = nodeList.item(0);
            return node.getNodeValue();
        } catch (Exception e) {
            return null;
        }
    }
}

