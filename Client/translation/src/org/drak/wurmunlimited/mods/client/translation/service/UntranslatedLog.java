package org.drak.wurmunlimited.mods.client.translation.service;

import org.drak.wurmunlimited.mods.client.translation.Translation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.drak.wurmunlimited.mods.client.translation.Translation.getJarDir;

/**
 * Created by Drak on 19.09.2018.
 */
public class UntranslatedLog extends  Vocabulary {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());

    public UntranslatedLog(String filename, String language) {
        super(filename, language);
        filePath = getJarDir() + "\\" + filename;
        tags = new String[]{"text", "dictionary"};
        loadFile();
    }

    public void saveFile() {
        int counter = 0;
        File xmlFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        String FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";
        try {
            dbFactory.setFeature(FEATURE, true);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // create the root element
            Element mainRoot = doc.createElement("Root");
            doc.appendChild(mainRoot);
            for(Map.Entry<String, String> entry : dic.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                // create data elements and place them under root
                Element subElement0 = doc.createElement(tags[0]);
                Element subElement1 = doc.createElement(tags[1]);
                subElement0.appendChild(doc.createTextNode(key));
                subElement1.appendChild(doc.createTextNode(value));
                Element rowElement = doc.createElement("Row");
                rowElement.appendChild(subElement0);
                rowElement.appendChild(subElement1);
                mainRoot.appendChild(rowElement);
                counter += counter;
            }
            // output DOM XML to console
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            DOMSource source = new DOMSource(doc);
            //StreamResult console = new StreamResult(System.out);
            //transformer.transform(source, console);
            transformer.transform(source, new StreamResult(new FileOutputStream(xmlFile)));

            //System.out.println("\nXML DOM Created Successfully..");

        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void outLoadResult(int counter, String filename) {
        logger.log(Level.INFO, "Loaded " + counter + " untranslated rows from \'" + filename + "\'");
    }

    public void removeAlreadyTranslated(Vocabularies vocabularies){
        Map<String, Vocabulary> existVocabularies = new HashMap<String, Vocabulary>();
        Field fields[] = vocabularies.getClass().getDeclaredFields();
        // Get all fields that are Vocabulary.
        for(Field field : fields){
            if (field.getType().toString().equals(Vocabulary.class.toString())) {
                try {
                    Vocabulary vocabulary = (Vocabulary) field.get(vocabularies);
                    existVocabularies.put(vocabulary.getFilename(), vocabulary);
                } catch (Exception e){
                    logger.log(Level.WARNING, e.getMessage(), e);
                }
            }
        }
        boolean changed = false;
        // Checks all records in untranslated.xml (dic field here) and remove if it exists in used dictionaries.
        for(Iterator<Map.Entry<String, String>> it = dic.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, String> entry = it.next();
            String key = entry.getKey();
            String value = entry.getValue();
            String[] usedDics = value.split(", ");
            for (String usedDic : usedDics) {
                if (existVocabularies.containsKey(usedDic)){
                    if (existVocabularies.get(usedDic).getDic().containsKey(key)){
                        changed = true;
                        it.remove();
                    }
                }
            }
        }
        if (changed) saveFile();
    }
}
