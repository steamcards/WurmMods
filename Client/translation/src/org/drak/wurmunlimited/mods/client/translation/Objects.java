package org.drak.wurmunlimited.mods.client.translation;

import com.wurmonline.client.renderer.ObjectData;
import com.wurmonline.client.util.SecureStrings;
import org.drak.wurmunlimited.mods.client.translation.service.Vocabulary;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.gotti.wurmunlimited.modloader.ReflectionUtil.getField;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.getPrivateField;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.setPrivateField;

import static org.drak.wurmunlimited.mods.client.translation.Translation.*;

/**
 * Created by Drak on 02.09.2018.
 */
public class Objects implements WurmClientMod {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());


    public Objects() {

        objectsOnGroundHook();
        tilePickerHook();
    }

    //TODO: objectsOnGroundHook
    void objectsOnGroundHook() {
        // items on the ground
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.ObjectData", "updateDisplayName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            SecureStrings name = getPrivateField(object, getField(ObjectData.class, "name"));
                            // change baseName before invoke method
                            String transName = getItemTranslate(name.toString(), new Vocabulary[]{translated.items, translated.creatures});

                            if (!transName.equals("")) {
                                setPrivateField(object, getField(ObjectData.class, "name"), new SecureStrings(transName));
                                //logger.log(Level.INFO, name.toString());
                                result = method.invoke(object, args);
                                // set baseName back
                                setPrivateField(object, getField(ObjectData.class, "name"), new SecureStrings(name.toString()));
                                // capitalizeFirstLetter
                                SecureStrings displayName = getPrivateField(object, getField(ObjectData.class, "displayName"));
                                setPrivateField(object, getField(ObjectData.class, "displayName"), new SecureStrings(capitalizeFirstLetter(displayName.toString())));
                            } else {
                                result = method.invoke(object, args);
                            }
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });

        // material of items (added a check of word roots for cyrillic and similar languages used changing the form of adjectives for gender)
        HookManager.getInstance().registerHook("com.wurmonline.shared.util.MaterialUtilities", "appendNameWithMaterial", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            StringBuilder sb = (StringBuilder) args[0];
                            String baseName = (String) args[1];
                            byte materialId = (byte) args[2];
                            String materialName = com.wurmonline.shared.util.MaterialUtilities.getClientMaterialString(materialId, true);
                            String transName = (materialName == null) ? materialName : getTranslate(materialName, new Vocabulary[]{translated.roots});
                            if (materialName != null && baseName.length() != 0 && !baseName.contains(transName) && baseName.charAt(0) != 34) {
                                sb.append(baseName.trim());
                                sb.append(", ");
                                materialName = getTranslate(materialName, new Vocabulary[]{translated.materials});
                                sb.append(materialName);
                            } else {
                                sb.append(baseName);
                            }
                            //logger.log(Level.INFO, "getClientMaterialString: " + result);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
    }


    //TODO: tilePickerHook
    void tilePickerHook() {
        //
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.cave.CaveWallPicker", "getHoverName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        String result;
                        try {
                            result = tilePickerTranslate((String) method.invoke(object, args));
                            return result;

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = (String) method.invoke(object, args);
                            return result;
                        }
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.TilePicker", "getHoverName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        String result;
                        try {
                            result = tilePickerTranslate((String) method.invoke(object, args));
                            return result;

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = (String) method.invoke(object, args);
                            return result;
                        }
                    }
                };
            }
        });
    }


}
