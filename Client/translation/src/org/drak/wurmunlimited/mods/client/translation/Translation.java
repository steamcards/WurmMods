package org.drak.wurmunlimited.mods.client.translation;

import java.awt.*;
import java.io.File;
import java.lang.reflect.*;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.wurmonline.client.game.World;
import com.wurmonline.client.renderer.gui.*;
import com.wurmonline.client.renderer.gui.text.SimpleTextFont;
import com.wurmonline.client.renderer.gui.text.TextFont;

import javassist.Modifier;
import org.drak.wurmunlimited.mods.client.translation.service.UntranslatedLog;
import org.drak.wurmunlimited.mods.client.translation.service.Vocabularies;
import org.drak.wurmunlimited.mods.client.translation.service.Vocabulary;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.Initable;
import org.gotti.wurmunlimited.modloader.interfaces.PreInitable;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;
import com.wurmonline.client.game.inventory.*;

import static org.gotti.wurmunlimited.modloader.ReflectionUtil.*;

/**
 * Created by Drak on 20.02.2018.
 */
public class Translation
        implements WurmClientMod, Configurable, Initable, PreInitable {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());

    public static World world;
    public static HeadsUpDisplay hud;

    public static Vocabularies translated;
    private static UntranslatedLog untranslated;
    private static String language;
    private static boolean saveUntranslated = false;

    public static void main(String[] args) {
        /*
        untranslated = new UntranslatedLog("untranslated.xml", language);
        Vocabulary creatures = new Vocabulary("creatures.xml", language);
        creatures.loadFile();
        untranslated.dic = creatures.dic;
        untranslated.saveFile();
        */
    }

    @Override
    public void configure(Properties properties) {
        String info = "Translation mod for Wurm Unlimited Client by Alex \'Drak\' Simson, e-mail:steamcards@gmail.com";
        logger.log(Level.INFO, info);
        logger.log(Level.INFO, "Initializing translation module...");

        language = properties.getProperty("language", "RU");
        saveUntranslated = Boolean.valueOf(properties.getProperty("save.untranslated", Boolean.toString(saveUntranslated)));
        translated = new Vocabularies(language);
        untranslated = new UntranslatedLog("untranslated_" + language + ".xml", language);
        untranslated.removeAlreadyTranslated(translated);
    }


    @Override
    public void preInit() {
        try {

        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")

    @Override
    public void init() {
        serviceHook();

        Objects objects = new Objects();
        Craft craft = new Craft();
        Structures structures = new Structures();
        Inventory inventory = new Inventory();
        MenuImprove menuImprove = new MenuImprove();
        GameInterface gameInterface = new GameInterface();
        Cooking cooking = new Cooking();
        Skills skills = new Skills();
        PaperDoll paperDoll = new PaperDoll();
    }

    //TODO: serviceHook
    void serviceHook() {
        // Global variable.
        HookManager.getInstance().registerHook("com.wurmonline.client.game.World", "setHud", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            world = (World) object;
                            hud = (HeadsUpDisplay) args[0];
                            result = method.invoke(object, args);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });


        // change the font for SelectBar
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.SelectBarIronRenderer", "init", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Constructor<?> simpleConstructor = SimpleTextFont.class.getDeclaredConstructor(Font.class, boolean.class);
                            simpleConstructor.setAccessible(true);
                            TextFont tf = (TextFont) simpleConstructor.newInstance(new Font("SansSerif", Font.PLAIN, 14), true);
                            setPrivateField(object, getField(SelectBarIronRenderer.class, "text"), tf);
                            result = method.invoke(object, args);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });

        // change TextFont on a window load
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.CreationWindow", "load", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            //the client have a bug with displaying cyrillic symbols in used font, so we change the font in the crafting tree
                            WurmTreeList createItemList = getPrivateField(object, getField(CreationWindow.class, "createItemList"));
                            WurmTreeList unfinishedItemList = getPrivateField(object, getField(CreationWindow.class, "unfinishedItemList"));
                            Constructor<?> simpleConstructor = SimpleTextFont.class.getDeclaredConstructor(Font.class, boolean.class);
                            simpleConstructor.setAccessible(true);
                            TextFont tf = (TextFont) simpleConstructor.newInstance(new Font("SansSerif", Font.PLAIN, 14), true);
                            createItemList.changeText(tf);
                            unfinishedItemList.changeText(tf);
                            result = method.invoke(object, args);
                            //setPrivateField(object, getField(CreationWindow.class, "resizable"), true);
                            //logger.log(Level.INFO, "name HERE: " + getPrivateField(object, getField(CreationWindow.class, "resizable")));
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
    }

    //TODO: tilePickerTranslate
    static String tilePickerTranslate(String result) {
        String suffix = "";
        int suffixStartPos = result.indexOf("(");
        if (suffixStartPos > -1) {
            int suffixEndPos = result.indexOf(")");
            suffix = result.substring(suffixStartPos + 1, suffixEndPos);
            if (suffix.contains("slope")) {
                suffix = suffix.replace("slope up", getTranslate("slope up", new Vocabulary[]{translated.tiles}));
                suffix = suffix.replace("slope down", getTranslate("slope down", new Vocabulary[]{translated.tiles}));
            } else {
                suffix = getSplitTranslate(suffix, new Vocabulary[]{translated.tiles, translated.materials, translated.prefixes});
            }
            suffix = " (" + suffix + ")";
            result = result.substring(0, suffixStartPos);
        }
        result = getTranslate(result, new Vocabulary[]{translated.tiles});
        return result + suffix;
    }

    //TODO: getFirstItemWithNameFromChildren
    static InventoryMetaItem getFirstItemWithNameFromChildren(List<InventoryMetaItem> children, String nameToFind, int itemNumber) {
        int itemToFindSize = 0;

        Vocabulary[] vocabularies = new Vocabulary[]{translated.items, translated.materials};
        for (int j = 0; j < children.size(); ++j) {
            InventoryMetaItem child = children.get(j);
            if (child != null) {
                if (child.getBaseName().equalsIgnoreCase(nameToFind)) {
                    if (itemToFindSize == itemNumber) {
                        return child;
                    }

                    ++itemToFindSize;
                } else if (child.getDisplayName().equalsIgnoreCase(getSplitTranslate(nameToFind, vocabularies))) {
                    if (itemToFindSize == itemNumber) {
                        return child;
                    }

                    ++itemToFindSize;
                }

                if (!child.getChildren().isEmpty()) {
                    InventoryMetaItem item = getFirstItemWithNameFromChildren(child.getChildren(), nameToFind, itemToFindSize);
                    if (item != null) {
                        return item;
                    }
                }
            }
        }
        return null;
    }

    // TODO: translateCreationList
    static Map<Short, CreationListItem> translateCreationList(Map<Short, CreationListItem> list) {
        Map<Short, CreationListItem> result = list;
        try {
            Vocabulary[] vocabularies = new Vocabulary[]{translated.items, translated.creatures};
            for (Short key : list.keySet()) {
                CreationListItem item = list.get(key);
                String itemName = getPrivateField(item, getField(CreationListItem.class, "itemName"));
                String transName = getItemTranslate(itemName, vocabularies);
                setPrivateField(item, getField(CreationListItem.class, "itemName"), capitalizeFirstLetter(transName));

                List<CreationListItem> childList = getPrivateField(item, getField(CreationListItem.class, "childList"));
                //logger.log(Level.INFO, "childList.size(): " + childList.size());
                for (int i = 0; i < childList.size(); i++) {
                    CreationListItem child = childList.get(i);
                    itemName = getPrivateField(item, getField(CreationListItem.class, "itemName"));
                    transName = getItemTranslate(itemName, vocabularies);
                    setPrivateField(child, getField(CreationListItem.class, "itemName"), transName);
                    childList.set(i, child);
                }
                setPrivateField(item, getField(CreationListItem.class, "childList"), childList);

                list.put(key, item);
                //logger.log(Level.INFO, "translateCreationList: " + transName);
            }
            result = list;
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
        return result;
    }

    // TODO: getSplitTranslate
    static String getSplitTranslate(String transName, Vocabulary[] vocabularies) {
        String[] splits = transName.split(", ");
        transName = "";
        for (int i = 0; i < splits.length; i++) {
            try {
                transName += getTranslate(splits[i], vocabularies);
                if (i + 1 != splits.length) transName += ", ";
            } catch (Exception e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }
        return transName;
    }

    // TODO: getSplitReverseTranslate
    static String getSplitReverseTranslate(String transName, Vocabulary[] vocabularies) {
        String[] splits = transName.split(", ");
        transName = "";
        for (int i = 0; i < splits.length; i++) {
            try {
                transName += getReverseTranslate(splits[i], vocabularies);
                if (i + 1 != splits.length) transName += ", ";
            } catch (Exception e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }
        return transName;
    }

    static String getReverseTranslate(String transName, Vocabulary vocabulary) {
        return getReverseTranslate(transName, new Vocabulary[] {vocabulary}, true);
    }

    static String getReverseTranslate(String transName, Vocabulary[] vocabularies) {
        return getReverseTranslate(transName, vocabularies, true);
    }

    static String getReverseTranslate(String transName, Vocabulary[] vocabularies, boolean doLog) {
        for (Vocabulary vocabulary : vocabularies) {
            for (String key : vocabulary.dic.keySet()) {
                if (transName.equalsIgnoreCase(vocabulary.dic.get(key.toLowerCase()))) {
                    return key;
                }
            }
        }
        if (doLog) logger.log(Level.INFO, "Reverse translation not found for: " + transName);
        return transName;
    }

    static String getTranslate(String origName, Vocabulary vocabulary) {
        return getTranslate(origName,  new Vocabulary[] {vocabulary});
    }

    static String getTranslate(String baseName, Vocabulary[] vocabularies) {
        baseName = baseName.trim();
        for (Vocabulary vocabulary : vocabularies) {
            if (vocabulary.getDic().containsKey(baseName.toLowerCase())) {
                //logger.log(Level.INFO, "baseName: " + baseName + " translation: " + dictionaries.get(baseName));
                return vocabulary.getDic().get(baseName.toLowerCase());
            }
        }
        saveUntranslatedToFile(baseName, vocabularies);
        return baseName;
    }

    static String getItemTranslate(String origName, Vocabulary vocabulary) {
        return getItemTranslate(origName,  new Vocabulary[] {vocabulary});
    }

    static String getItemTranslate(String origName, Vocabulary[] vocabularies) {
        String result = "";
        String baseName = origName.trim();
        boolean prefixFound = false;

        for (Vocabulary vocabulary : vocabularies) {
            if (vocabulary.getDic().containsKey(baseName.toLowerCase())) {
                //logger.log(Level.INFO, "baseName: " + baseName + " translation: " + vocabulary.get(baseName));
                result += vocabulary.getDic().get(baseName.toLowerCase());
                //break;
                return result;
            }
        }

        int startBracketPos = baseName.indexOf("(");
        if (startBracketPos > 0) {
            int endBracketPos = baseName.indexOf(")");
            if (endBracketPos > 0 && endBracketPos > startBracketPos) {
                String textInsideBrackets = baseName.substring(startBracketPos + 1, endBracketPos);
                result += getItemTranslate(baseName.replace("(" + textInsideBrackets + ")", ""), vocabularies);
                result += " (" + getSplitTranslate(textInsideBrackets, new Vocabulary[]{translated.prefixes}) + ")";
                return result;
            }
        }

        for (String key : translated.prefixes.getDic().keySet()) {
            String k = key.toLowerCase();
            if (baseName.toLowerCase().indexOf(k + " ") == 0) {
                prefixFound = true;
                //logger.log(Level.INFO, "key prefixFound: " + k + " baseName " + baseName);
                result = translated.prefixes.getDic().get(k) + " ";
                baseName = lowerCaseReplace(baseName, k, "").trim();
                break;
            }
        }
        if (prefixFound) {
            result += (baseName.length() > 0) ? getItemTranslate(baseName, vocabularies) : "";
        } else {
            result += baseName;
            saveUntranslatedToFile(origName, vocabularies);
        }
        return result;
    }

    public static String getJarDir() {
        String result = null;
        CodeSource codeSource = Translation.class.getProtectionDomain().getCodeSource();
        File jarFile = null;
        try {
            jarFile = new File(codeSource.getLocation().toURI().getPath());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        if (jarFile != null) {
            result = jarFile.getParentFile().getPath();
        }
        return result;
    }

    private static void saveUntranslatedToFile(String string, Vocabulary[] vocabularies) {
        if (!saveUntranslated) return;
        String voc_names = "";
        for (int i = 0; i < vocabularies.length; i++) {
            if (i == 0) {
                voc_names += vocabularies[i].getFilename();
            } else {
                voc_names += ", " + vocabularies[i].getFilename();
            }
        }
        try {
            if (!untranslated.dic.containsKey(string.toLowerCase())) {
                String caller = getCaller();//"Translate";
                logger.log(Level.INFO, "[" + caller + "] Service translation not found for: \"" + string + "\"");
                untranslated.dic.put(string, voc_names);
                untranslated.saveFile();
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    public static boolean containsIgnoreCase(String src, String what) {
        return src.toLowerCase().contains(what.toLowerCase());
    }

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

    public static String lowerCaseReplace(String source, String target, String replacement) {
        StringBuilder sbSource = new StringBuilder(source);
        StringBuilder sbSourceLower = new StringBuilder(source.toLowerCase());
        String searchString = target.toLowerCase();

        int idx = 0;
        while ((idx = sbSourceLower.indexOf(searchString, idx)) != -1) {
            sbSource.replace(idx, idx + searchString.length(), replacement);
            sbSourceLower.replace(idx, idx + searchString.length(), replacement);
            idx += replacement.length();
        }
        sbSourceLower.setLength(0);
        sbSourceLower.trimToSize();
        sbSourceLower = null;

        return sbSource.toString();
    }

    public static Method getRecursiveMethod(Class clazz, Class[] checkParams, String strMethod) {
        //Class<?> clazz = plugin.getClass();
        while (clazz != null) {
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                if (method.getName().equals(strMethod)) {
                    // Test any other things about it beyond the name...
                    if (checkParams == null) {
                        //logger.log(Level.INFO, ">>> no params <<< ");
                        return method;
                    }
                    else {
                        Class[] methodParams = new Class[method.getParameters().length];
                        for (int i = 0; i < method.getParameters().length; i++) {
                            methodParams[i] = method.getParameters()[i].getType();
                        }
                        //logger.log(Level.INFO, ">>> method.getParameters().length <<< " + method.getParameters().length + " <<<" + ">>" + Arrays.toString(methodParams) + " <<< inc param: " + Arrays.toString(checkParams) + ">>" + Arrays.equals(methodParams, checkParams) + " >> " + method.getName() + " <--->  " + strMethod);
                        if (Arrays.equals(methodParams, checkParams)) {
                            //logger.log(Level.INFO, ">>> parameters <<< " + Arrays.toString(methodParams) + ">>" + Arrays.toString(checkParams) + " <<<");
                            return method;

                        }
                    }
                }
            }
            clazz = clazz.getSuperclass();
        }
        return null;
    }

    public static Field getRecursiveField(Class clazz, String strField) {
        while (clazz != null) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                // Test any other things about it beyond the name...
                if (field.getName().equals(strField)) {
                    return field;
                }
            }
            clazz = clazz.getSuperclass();
        }
        return null;
    }

    public static Object getFieldValue(Object object, String fieldName) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        Field field = getRecursiveField(object.getClass(), fieldName);
        field.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        return field.get(object);
        //return getPrivateField(fieldInstance, field);
    }

    public static void setFieldValue(Object object, String fieldName, Object value) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        Field field = getRecursiveField(object.getClass(), fieldName);
        field.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        setPrivateField(object, field, value);
    }

    public static Object callMethod(Object object, Field field, String methodName, Object[] args) {
        return callMethod(object, null, field, methodName, args);
    }

    public static Object callMethod(Object object, Class[] parameters, Field field, String methodName, Object[] args) {
        try {
            field.setAccessible(true);
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
            Class fieldClass = Class.forName(field.getType().getName());
            Object fieldInstance = field.get(object);
            Method method = Translation.getRecursiveMethod(fieldClass, parameters, methodName);
            //logger.log(Level.INFO, ">>> fieldInstance <<< " + method  + ">>" + fieldInstance + " <<< >>> " + Arrays.toString(args) + "<<");
            method.setAccessible(true);
            return method.invoke(fieldInstance, args);
        }
        catch ( Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
        return null;
    }

    static String compareCaller(String... methodName) {
        String caller = "";
        for (int i = 0; i < Thread.currentThread().getStackTrace().length; i++) {
            caller += "[" + Thread.currentThread().getStackTrace()[i].getMethodName() + "]";
        }

        return caller;
    }


    static String getCaller() {
        String caller = "";
        for (int i = 0; i < Thread.currentThread().getStackTrace().length; i++) {
            caller += "[" + Thread.currentThread().getStackTrace()[i].getMethodName() + "]";
        }

        return caller;
    }
}

