/**
 * Created by Drak on 11.03.2018.
 */

package org.drak.wurmunlimited.mods.client.translation;

import com.wurmonline.client.game.inventory.InventoryMetaItem;
import com.wurmonline.client.game.inventory.InventoryMetaWindowView;
import com.wurmonline.client.renderer.gui.*;
import com.wurmonline.shared.constants.PlayerAction;
import com.wurmonline.shared.constants.PlayerActionConstants;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.Initable;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.gotti.wurmunlimited.modloader.ReflectionUtil.*;

public class MenuImprove implements WurmClientMod {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());
    private InventoryMetaItem[] inventoryMetaItems;

    public MenuImprove() {

        menuHook();
    }


    void menuHook() {

        // set flag to true if clicked from inventory
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.InventoryListComponent$InventoryTreeListItem", "rightClick", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            if (inventoryMetaItems == null) inventoryMetaItems = new InventoryMetaItem[]{null};
                            InventoryMetaItem item = getPrivateField(object, getField(object.getClass(), "item"));
                            inventoryMetaItems[0] = item;
                            //logger.log(Level.INFO, "isInInventory: " + inventoryMetaItems[0].getDisplayName());
                            result = method.invoke(object, args);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
        //
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.HeadsUpDisplay", "popupReceived", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            if (inventoryMetaItems == null) inventoryMetaItems = new InventoryMetaItem[]{null};
                            result = method.invoke(object, args);
                            inventoryMetaItems[0] = null;

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
        //add action to menu
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.HeadsUpDisplay", "showPopupData", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            //private void showPopupData(final WurmPopup rootPopup, List<PlayerAction> actionList, String helpTopic, Map<String, WurmPopup> oldPopups, boolean grayedOut)
                            List<PlayerAction> actionList = (List<PlayerAction>) args[1];
                            if (actionList != null) {
                                boolean isCombineInList = false;
                                for (PlayerAction playerAction : actionList) {
                                    //logger.log(Level.INFO, "playerAction: " + playerAction.getName());
                                    if (playerAction.getId() == PlayerActionConstants.CMD_COMBINE) {
                                        isCombineInList = true;
                                        break;
                                    }
                                }
                                if (!isCombineInList) {
                                    HeadsUpDisplay hud = (HeadsUpDisplay)object;
                                    //logger.log(Level.INFO, "compare: " + isInInventory(inventoryMetaItems[0], hud));
                                    InventoryListComponent inventoryListComponent = hud.getInventoryWindow().getInventoryListComponent();
                                    Class<?> singleTreeListItemClass = Class.forName("com.wurmonline.client.renderer.gui.InventoryListComponent$InventoryTreeListItem");
                                    Object rootItem = getPrivateField(inventoryListComponent, getField(InventoryListComponent.class, "rootItem"));
                                    InventoryMetaItem root = getPrivateField(rootItem, getField(singleTreeListItemClass, "item"));

                                    if (isInInventory(inventoryMetaItems[0], root)) {
                                        PlayerAction playerAction = new PlayerAction(PlayerActionConstants.CMD_COMBINE, PlayerAction.INVENTORY_ITEM, "Combine", true);
                                        actionList.add(playerAction);
                                    }
                                }
                                //logger.log(Level.INFO, "AFTER: ");
                            }
                            result = method.invoke(object, args);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
        //execute the action
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.HeadsUpDisplay", "sendPopupAction", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            HeadsUpDisplay hud = (HeadsUpDisplay) object;
                            PlayerAction playerAction = (PlayerAction) args[0];

                            if (playerAction != null && playerAction.getId() == PlayerActionConstants.CMD_COMBINE) {
                                long[] popupTargets = getPrivateField(object, getField(HeadsUpDisplay.class, "popupTargets"));
                                hud.getWorld().getServerConnection().sendAction(popupTargets[0], popupTargets, PlayerAction.COMBINE);
                            } else {
                                result = method.invoke(object, args);
                            }

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

    }


    public boolean isInInventory(InventoryMetaItem patternItem, InventoryMetaItem root)
    {
        if (patternItem == null) return false;
        boolean result = false;
        try {
                if (root != null) {
                    for (int i = 0; i < root.getChildren().size(); ++i) {
                        InventoryMetaItem item = (InventoryMetaItem) root.getChildren().get(i);
                        if (item.getId() == patternItem.getId()) {
                            //logger.log(Level.INFO, "item: " + item.getDisplayName());
                            result = true;
                            break;
                        } else {
                            result = isInInventory(patternItem, item);
                            if (result) break;
                        }
                    }
                }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //logger.log(Level.INFO, "result: " + result);
        return result;
    }

}
