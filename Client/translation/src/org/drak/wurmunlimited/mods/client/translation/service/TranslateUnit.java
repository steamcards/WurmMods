package org.drak.wurmunlimited.mods.client.translation.service;

/**
 * Created by Drak on 26.02.2018.
 */
public class TranslateUnit {
    private String original;
    private String translated;

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getTranslated() {
        return translated;
    }

    public void setTranslated(String translated) {
        this.translated = translated;
    }
}