package org.drak.wurmunlimited.mods.client.translation;

import com.wurmonline.client.WurmClientBase;
import com.wurmonline.client.game.SkillLogic;
import com.wurmonline.client.game.SkillLogicSet;
import com.wurmonline.client.renderer.gui.CreationListItem;
import com.wurmonline.client.renderer.gui.CreationListWindow;
import com.wurmonline.client.renderer.gui.HeadsUpDisplay;
import com.wurmonline.client.renderer.gui.WurmComponent;
import org.drak.wurmunlimited.mods.client.translation.service.Vocabulary;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.gotti.wurmunlimited.modloader.ReflectionUtil.*;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.callPrivateMethod;

import static org.drak.wurmunlimited.mods.client.translation.Translation.*;

/**
 * Created by Drak on 02.09.2018.
 */
public class Craft implements WurmClientMod {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());

    public Craft() {

        craftHook();
        craftingRecipesHook();
    }


    //TODO: craftHook
    void craftHook() {
        // translated category in a list
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.CreationWindow", "setCurrentCategory", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            Vocabulary[]vocabularies = new Vocabulary[]{translated.skills, translated.items, translated.creatures};
                            String categoryName = (String) args[0];
                            String transName = getItemTranslate(categoryName, vocabularies);
                            args[0] = capitalizeFirstLetter(transName);
                            result = method.invoke(object, args);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        // translated items in a list
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.CreationWindow", "AddCreationItem", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            Vocabulary[]vocabularies = new Vocabulary[]{translated.items, translated.creatures};
                            String itemName = (String) args[0];
                            // split to name and material
                            String[] splits = itemName.split(", ");
                            String transName = getItemTranslate(splits[0], vocabularies);
                            for (int i = 1; i < splits.length; i++) {
                                transName += ", ";
                                transName += getTranslate(splits[i], vocabularies);
                            }

                            args[0] = transName;
                            //logger.log(Level.INFO, "name HERE: " + args[0]);
                            result = method.invoke(object, args);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
    }

    //TODO: craftingRecipesHook
    void craftingRecipesHook() {
        // crafting categories
        //HookManager.getInstance().registerHook("com.wurmonline.client.comm.ServerConnectionListenerClass", "addCreationCategoryLists", null, new InvocationHandlerFactory() {
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.CreationListWindow", "addCategoryCreationLists", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            Vocabulary[]vocabularies = new Vocabulary[]{translated.items};

                            CreationListItem startingItems = getPrivateField(object, getField(CreationListWindow.class, "startingItems"));
                            CreationListItem additionalItems = getPrivateField(object, getField(CreationListWindow.class, "additionalItems"));

                            String startingItemsName = getPrivateField(startingItems, getField(CreationListItem.class, "itemName"));
                            String additionalItemsName = getPrivateField(additionalItems, getField(CreationListItem.class, "itemName"));

                            setPrivateField(startingItems, getField(CreationListItem.class, "itemName"), getTranslate(startingItemsName, vocabularies));
                            setPrivateField(additionalItems, getField(CreationListItem.class, "itemName"), getTranslate(additionalItemsName, vocabularies));

                            Map<Short, CreationListItem> categoryList = (Map<Short, CreationListItem>) args[0];
                            categoryList = translateCreationList(categoryList);

                            args[0] = categoryList;
                            result = method.invoke(object, args);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        // translated a crafting item
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.CreationListWindow", "addItemToCreationList", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            CreationListItem item = (CreationListItem) args[0];
                            String itemName = getPrivateField(item, getField(CreationListItem.class, "itemName"));

                            String[] splits = itemName.split(", ");
                            String transName = "";
                            Vocabulary[]vocabularies = new Vocabulary[]{translated.materials, translated.items};
                            for (int i = 0; i < splits.length; i++) {
                                transName += getTranslate(splits[i], vocabularies);
                                if (i + 1 != splits.length) transName += ", ";
                            }
                            setPrivateField(item, getField(CreationListItem.class, "itemName"), transName);
                            args[0] = item;
                            //logger.log(Level.INFO, "addItemToCreationList: " + transName);
                            result = method.invoke(object, args);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        // crafting components
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.CreationListItem", "addChild", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            CreationListItem item = (CreationListItem) args[0];
                            String itemName = getPrivateField(item, getField(CreationListItem.class, "itemName"));

                            // split to name and material
                            String[] splits = itemName.split(", ");
                            String transName = "";
                            Vocabulary[]vocabularies = new Vocabulary[]{translated.materials, translated.items};
                            for (int i = 0; i < splits.length; i++) {
                                transName += getTranslate(splits[i], vocabularies);
                                if (i + 1 != splits.length) transName += ", ";
                            }
                            setPrivateField(item, getField(CreationListItem.class, "itemName"), transName);

                            String skillName = (String) getFieldValue(object, "skill");
                            // If the skill already translated and found don't translate the current skill.
                            SkillLogic skill = SkillLogicSet.getSkill(skillName);
                            if (skill != null) {
                                skillName = getTranslate(skillName, translated.skills);
                                setFieldValue(object, "skill", skillName);
                            }
                            args[0] = item;
                            //logger.log(Level.INFO, "addChild: " + transName);
                            result = method.invoke(object, args);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        // translation of input for calling method by right click
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.CreationListWindow", "addItemPopupWindow", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            String itemName = (String) args[2];
                            Vocabulary[]vocabularies = new Vocabulary[]{translated.items, translated.materials};
                            String sourceItemName = getSplitReverseTranslate((String) args[3], vocabularies);
                            String targetItemName = getSplitReverseTranslate((String) args[4], vocabularies);

                            String[] splits = itemName.split(", "); //BUG FIX: remove material name from item name for correct search (not best solution)

                            sourceItemName = (sourceItemName.contains("lump")) ? formatLumps(sourceItemName) : sourceItemName;
                            targetItemName = (targetItemName.contains("lump")) ? formatLumps(targetItemName) : targetItemName;

                            args[2] = splits[0];
                            args[3] = sourceItemName;
                            args[4] = targetItemName;
                            result = method.invoke(object, args);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        // translation of input for calling method by right click
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.CreationListWindow", "getHelpString", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        try {
                            Vocabulary[]vocabularies = new Vocabulary[]{translated.items, translated.materials};
                            String untransItemName = getSplitReverseTranslate((String) args[0], vocabularies);
                            args[0] = untransItemName;
                            return method.invoke(object, args);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            return method.invoke(object, args);
                        }
                    }
                };
            }
        });

        // reverse translated for double click menu (search, tooltip)
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.CreationListItem", "doubleClick", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            //int xMouse = (int)args[0];
                            //int yMouse = (int)args[1];

                            CreationListWindow creationListWindow = getPrivateField(object, getField(CreationListItem.class, "creationListWindow"));
                            boolean isCategory = getPrivateField(object, getField(CreationListItem.class, "isCategory"));
                            String itemName = getPrivateField(object, getField(CreationListItem.class, "itemName"));
                            HeadsUpDisplay hud = getPrivateField(object, getField(WurmComponent.class, "hud"));

                            if (creationListWindow != null) {
                                Method setSearchText = creationListWindow.getClass().getDeclaredMethod("setSearchText", String.class);
                                Method rebuildListFilteredByCategory = creationListWindow.getClass().getDeclaredMethod("rebuildListFilteredByCategory", String.class);
                                Method didFindInputEqualsWithoutMaterial = creationListWindow.getClass().getDeclaredMethod("didFindInputEqualsWithoutMaterial", String.class);
                                Method rebuildListFilteredByItemEqualsWithoutMaterial = creationListWindow.getClass().getDeclaredMethod("rebuildListFilteredByItemEqualsWithoutMaterial", String.class);

                                //creationListWindow.setSearchText(itemName);
                                callPrivateMethod(creationListWindow, setSearchText, itemName);
                                if (isCategory) {
                                    callPrivateMethod(creationListWindow, rebuildListFilteredByCategory, itemName);
                                } else {
                                    boolean findAnything = callPrivateMethod(creationListWindow, didFindInputEqualsWithoutMaterial, itemName);
                                    if (findAnything) {
                                        callPrivateMethod(creationListWindow, rebuildListFilteredByItemEqualsWithoutMaterial, itemName);
                                    } else {
                                        Vocabulary[]vocabularies = new Vocabulary[]{translated.items, translated.materials};
                                        String transName = getSplitReverseTranslate(itemName, vocabularies);
                                        String mappings = "plonk.recipes." + transName.replaceAll(" ", "");
                                        String bmlString = WurmClientBase.getResourceManager().getResourceAsString(mappings);
                                        hud.getPlonkViewer().showPlonk(itemName, bmlString, 400, 300);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
    }


    // TODO: formatLumps
    private String formatLumps(String input) {
        String result = input;
        if (input.lastIndexOf(" ") != -1) {
            result = input.substring(input.lastIndexOf(" "));
            result = result.trim();
            result = result + ", ";
            result = result + input.substring(0, input.lastIndexOf(" "));
        }

        return result;
    }

}
