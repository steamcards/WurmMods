package org.drak.wurmunlimited.mods.client.translation;

import org.drak.wurmunlimited.mods.client.translation.service.Vocabulary;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.drak.wurmunlimited.mods.client.translation.Translation.*;

/**
 * Created by Drak on 02.09.2018.
 */
public class Structures implements WurmClientMod {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());


    public Structures() {

        structureHook();
    }


    //TODO: structureHook
    void structureHook() {
        // WallData
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.structures.WallData", "getHoverName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            result = method.invoke(object, args);
                            result = getItemTranslate((String)result, new Vocabulary[]{translated.items, translated.creatures});
                            //logger.log(Level.INFO, "getHoverName: " + result);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });

        // RoofData
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.structures.RoofData", "getHoverName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            result = method.invoke(object, args);
                            result = getItemTranslate((String)result, new Vocabulary[]{translated.items, translated.creatures});
                            //logger.log(Level.INFO, "RoofData: " + result);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });

        // MineDoorData
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.structures.MineDoorData", "getHoverName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            result = method.invoke(object, args);
                            result = getItemTranslate((String)result, new Vocabulary[]{translated.items, translated.creatures});
                            logger.log(Level.INFO, "MineDoorData: " + result);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });

        // HouseData
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.structures.HouseData", "getHoverName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            result = method.invoke(object, args);
                            result = getItemTranslate((String)result, new Vocabulary[]{translated.items, translated.creatures});
                            logger.log(Level.INFO, "HouseData: " + result);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });

        // FenceData
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.structures.FenceData", "getHoverName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            result = method.invoke(object, args);
                            result = getItemTranslate((String)result, new Vocabulary[]{translated.items, translated.creatures});
                            //logger.log(Level.INFO, "FenceData: " + result);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });

        // FloorData
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.structures.FloorData", "getHoverName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            result = method.invoke(object, args);
                            result = getItemTranslate((String)result, new Vocabulary[]{translated.items, translated.creatures});
                            //logger.log(Level.INFO, "FloorData: " + result);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });

        // BridgeData
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.structures.BridgeData", "getHoverName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            result = method.invoke(object, args);
                            result = getItemTranslate((String)result, new Vocabulary[]{translated.items, translated.creatures});
                            //logger.log(Level.INFO, "BridgeData: " + result);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });

        // BridgeData
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.SubPickableUnit", "getHoverName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            result = method.invoke(object, args);
                            result = getItemTranslate((String)result, new Vocabulary[]{translated.items, translated.creatures});
                            //logger.log(Level.INFO, "SubPickableUnit: " + result);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });
    }

}
