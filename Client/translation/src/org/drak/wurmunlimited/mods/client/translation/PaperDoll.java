package org.drak.wurmunlimited.mods.client.translation;

import com.wurmonline.client.game.SkillLogicSet;
import com.wurmonline.client.game.inventory.InventoryMetaItem;
import com.wurmonline.client.renderer.PickData;
import com.wurmonline.client.renderer.gui.PaperDollItem;
import com.wurmonline.client.renderer.gui.PaperDollSlot;
import org.drak.wurmunlimited.mods.client.translation.service.Vocabulary;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.drak.wurmunlimited.mods.client.translation.Translation.*;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.callPrivateMethod;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.getMethod;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.getPrivateField;

public class PaperDoll implements WurmClientMod {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());
    private boolean alreadyInit = false;

    public PaperDoll() {
        try {
            paperDoll();
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    //TODO: paper doll
    void paperDoll() throws Exception {
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.PaperDollInventory", "pick", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Vocabulary[] vocabularies = new Vocabulary[]{translated.gui, translated.items};
                            PickData pickData = (PickData) args[0];
                            int xMouse = (int) args[1];
                            int yMouse = (int) args[2];

                            boolean showPlayer = (boolean) getFieldValue(object, "showPlayer");
                            Method getWoundSlotAt = getMethod(object.getClass(), "getWoundSlotAt");
                            Method getWoundAt = getMethod(object.getClass(), "getWoundAt");
                            Method getItemAt = getMethod(object.getClass(), "getItemAt");
                            Method getFrameAt = getMethod(object.getClass(), "getFrameAt");
                            Method isItemHovered = getMethod(object.getClass(), "isItemHovered");
                            Method isHovered = getMethod(object.getClass(), "isHovered");
                            PaperDollSlot slot =  callPrivateMethod(object, getWoundSlotAt, new Object[]{xMouse, yMouse});
                            PaperDollItem item;
                            PaperDollSlot workingHandItem = (PaperDollSlot) getFieldValue(object, "workingHandItem");
                            PaperDollSlot bodyItem = (PaperDollSlot) getFieldValue(object, "bodyItem");
                            PaperDollSlot equippedWeightItem = (PaperDollSlot) getFieldValue(object, "equippedWeightItem");
                            InventoryMetaItem inventoryItem = (InventoryMetaItem) getFieldValue(object, "inventoryItem");
                            String itemName;

                            String DAMAGE = getTranslate("Damage", vocabularies);
                            String WOUNDS = getTranslate("wounds", vocabularies);
                            String QL = getTranslate("QL", vocabularies);
                            String HANDS = getTranslate("hands", vocabularies);
                            String EQUIPMENT_WEIGHT = getTranslate("equipment weight", vocabularies);
                            String INVENTORY_WEIGHT = getTranslate("inventory weight", vocabularies);
                            String TOTAL_WEIGHT = getTranslate("total weight", vocabularies);
                            String BODY_STRENGTH = "Body strength";
                            String SPEED_LIMIT = getTranslate("speed limit", vocabularies);
                            String MAX_WEIGHT = getTranslate("max weight", vocabularies);
                            String SHOW_WOUNDS = getTranslate("show wounds", vocabularies);
                            String SHOW_PLAYER = getTranslate("show player", vocabularies);


                            if(!showPlayer) {
                                item = callPrivateMethod(object, getWoundAt, new Object[]{xMouse, yMouse});
                                if(item != null) {
                                    pickData.reset();
                                    pickData.addText(item.getItemName() + " (" + item.getItem().getCustomName() + ")");
                                    pickData.addText(DAMAGE + ": " + item.getDamage());
                                    return this;
                                }

                                if(slot != null) {
                                    pickData.reset();
                                    if(slot.hasWounds()) {
                                        pickData.addText(slot.getItemName() + " " + WOUNDS);
                                        pickData.addText(DAMAGE + ": " + slot.getTotalWoundDamage());
                                        return this;
                                    }
                                }
                            }
                            item = callPrivateMethod(object, getItemAt, new Object[]{xMouse, yMouse});
                            if(item != null) {
                                pickData.reset();
                                pickData.addText(item.getItem().getDisplayName());
                                pickData.addText(QL + ": " + item.getQl());
                                pickData.addText(DAMAGE + ": " + item.getDamage());
                            } else {
                                slot = callPrivateMethod(object, getFrameAt, new Object[]{xMouse, yMouse});
                                if(slot != null) {
                                    pickData.reset();
                                    String bs1 = slot.getItemName();
                                    bs1 = getItemTranslate(bs1, translated.items);
                                    if(!slot.getItem().getCustomName().isEmpty()) {
                                        itemName = getItemTranslate(slot.getItem().getCustomName(), translated.items);
                                        bs1 = bs1 + " (" + itemName + ")";
                                    }

                                    pickData.addText(bs1);
                                //} else if(this.isItemHovered(this.workingHandItem, xMouse, yMouse)) {
                                } else if(callPrivateMethod(object, isItemHovered, new Object[]{workingHandItem, xMouse, yMouse})) {
                                    pickData.reset();
                                    pickData.addText(HANDS);
                                //} else if(this.isItemHovered(this.bodyItem, xMouse, yMouse)) {
                                } else if(callPrivateMethod(object, isItemHovered, new Object[]{bodyItem, xMouse, yMouse})) {
                                    pickData.reset();
                                    itemName = getItemTranslate(bodyItem.getItemName(), translated.items);
                                    pickData.addText(itemName);
                                //} else if(this.isItemHovered(this.equippedWeightItem, xMouse, yMouse)) {
                                } else if(callPrivateMethod(object, isItemHovered, new Object[]{equippedWeightItem, xMouse, yMouse})) {
                                    pickData.reset();
                                    pickData.addText(EQUIPMENT_WEIGHT + ": " + equippedWeightItem.getWeight());
                                    float bs;
                                    if(inventoryItem != null) {
                                        pickData.addText(INVENTORY_WEIGHT + ": " + inventoryItem.getWeight());
                                        bs = equippedWeightItem.getWeight() + inventoryItem.getWeight();
                                        pickData.addText(TOTAL_WEIGHT + ": " + bs);
                                    }

                                    try {
                                        bs = SkillLogicSet.getSkill(BODY_STRENGTH).getValue();
                                    } catch (Exception e) {
//                                        logger.log(Level.WARNING, "Skill \"" + skillName + "\" not found.");
                                        bs = 0;
                                    }
                                    double bsFloor = Math.floor((double)bs);
                                    pickData.addText("");
                                    pickData.addText("100% "+ SPEED_LIMIT + ": " + bsFloor * 2.0D);
                                    pickData.addText("75% " + SPEED_LIMIT + ": " + bsFloor * 3.5D);
                                    pickData.addText("25% " + SPEED_LIMIT + ": " + bsFloor * 7.0D);
                                    pickData.addText(MAX_WEIGHT + ": " + bs * 7.0F);
                                //} else if(this.isHovered(xMouse, yMouse, 59, 89, 36, 71)) {
                                } else if(callPrivateMethod(object, isHovered, new Object[]{xMouse, yMouse, 59, 89, 36, 71})) {
                                    pickData.reset();
                                    if(showPlayer) {
                                        pickData.addText(SHOW_WOUNDS);
                                    } else {
                                        pickData.addText(SHOW_PLAYER);
                                    }

                                }
                            }
                            result = this;
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
    }
}