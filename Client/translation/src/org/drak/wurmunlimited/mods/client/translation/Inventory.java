/**
 * Created by Drak on 02.09.2018.
 */

package org.drak.wurmunlimited.mods.client.translation;

import com.wurmonline.client.game.inventory.InventoryMetaItem;
import org.drak.wurmunlimited.mods.client.translation.service.Vocabulary;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.gotti.wurmunlimited.modloader.ReflectionUtil.getField;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.getPrivateField;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.setPrivateField;

import static org.drak.wurmunlimited.mods.client.translation.Translation.*;

public class Inventory implements WurmClientMod {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());

    public Inventory() {

        inventoryHook();
    }


    //TODO: inventoryHook
    void inventoryHook() {
        // items in the inventory
        HookManager.getInstance().registerHook("com.wurmonline.client.game.inventory.InventoryMetaItem", "updateDisplayName", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Vocabulary[]vocabularies = new Vocabulary[]{translated.items};
                            String baseName = getPrivateField(object, getField(InventoryMetaItem.class, "baseName"));
                            String transName = getItemTranslate(baseName, vocabularies);
                            if (!transName.equals("") && !baseName.equalsIgnoreCase("body")) { //exclude body from inventory
                                // change baseName before invoke method
                                setPrivateField(object, getField(InventoryMetaItem.class, "baseName"), transName);
                                result = method.invoke(object, args);
                                //String displayName = getPrivateField(object, getField(InventoryMetaItem.class, "displayName"));
                                //logger.log(Level.INFO, "baseName: " + baseName + " transName: " + transName + " displayName: " + displayName);
                                // set baseName back
                                setPrivateField(object, getField(InventoryMetaItem.class, "baseName"), baseName);
                            } else {
                                result = method.invoke(object, args);
                            }
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });


        // translated a rarity
        HookManager.getInstance().registerHook("com.wurmonline.shared.util.MaterialUtilities", "getRarityString", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Vocabulary[]vocabularies = new Vocabulary[]{translated.items};
                            String rarity = (String) method.invoke(object, args);
                            //logger.log(Level.INFO, "rarity: " + rarity);
                            result = getTranslate(rarity, vocabularies) + " ";

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        //a material of items (added a check of word roots for cyrillic and similar languages used changing the form of adjectives for gender)
        HookManager.getInstance().registerHook("com.wurmonline.shared.util.MaterialUtilities", "appendNameWithMaterialSuffix", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            StringBuilder sb = (StringBuilder) args[0];
                            String baseName = (String) args[1];
                            byte materialId = (byte) args[2];
                            String materialName = com.wurmonline.shared.util.MaterialUtilities.getClientMaterialString(materialId, true);
                            String transName = (materialName == null) ? null : getTranslate(materialName, new Vocabulary[]{translated.roots});
                            if (materialName != null && baseName.length() != 0 && !containsIgnoreCase(baseName, transName) && baseName.charAt(0) != 34) {
                                sb.append(baseName.trim());
                                sb.append(", ");
                                materialName = getTranslate(materialName, new Vocabulary[]{translated.materials});
                                sb.append(materialName);
                            } else {
                                sb.append(baseName);
                            }
                            //logger.log(Level.INFO, "getClientMaterialString: " + result);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        // fixing changes of DisplayName for added a craft items in the service method
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.InventoryListComponent", "getFirstItemWithNameFromChildren", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result = null;
                        try {
                            //private InventoryMetaItem getFirstItemWithNameFromChildren(List<InventoryMetaItem> children, String nameToFind, int itemNumber) {
                            List<InventoryMetaItem> children = (List<InventoryMetaItem>) args[0];
                            String nameToFind = (String) args[1];
                            int itemNumber = (int) args[2];
                            result = getFirstItemWithNameFromChildren(children, nameToFind, itemNumber);

                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
    }
}
