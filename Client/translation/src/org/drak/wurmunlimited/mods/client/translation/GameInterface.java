package org.drak.wurmunlimited.mods.client.translation;

import com.wurmonline.client.renderer.gui.InventoryWindow;
import com.wurmonline.client.renderer.gui.RecipeListWindow;
import org.drak.wurmunlimited.mods.client.translation.service.Vocabulary;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.classhooks.InvocationHandlerFactory;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.lang.reflect.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.drak.wurmunlimited.mods.client.translation.Translation.*;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.getField;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.getPrivateField;
import static org.gotti.wurmunlimited.modloader.ReflectionUtil.setPrivateField;

/**
 * Created by Drak on 03.09.2018.
 */
public class GameInterface implements WurmClientMod {
    private static final Logger logger = Logger.getLogger(Translation.class.getName());
    private boolean alreadyInit = false;


    public GameInterface() {
        try {
            gui();
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    //TODO: gui
    void gui() throws Exception {
        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.HeadsUpDisplay", "init", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            if (!alreadyInit) {
                                Vocabulary[] vocabularies = new Vocabulary[]{translated.gui, translated.items};
                                result = method.invoke(object, args);
                                Field[] fields = object.getClass().getDeclaredFields();
                                for (Field field : fields) {
                                    //processClasses(field, object, vocabularies);
                                    processSuperClasses(field, object, vocabularies);
                                }
                                RecipeListWindow recipeListWindow = getPrivateField(object, getField(object.getClass(), "recipeListWindow"));
                                fields = RecipeListWindow.class.getDeclaredFields();
                                for (Field field : fields) {
                                    processClasses(field, recipeListWindow, vocabularies);
                                }
                                InventoryWindow inventoryWindow = getPrivateField(object, getField(object.getClass(), "inventoryWindow"));
                                fields = InventoryWindow.class.getDeclaredFields();
                                for (Field field : fields) {
                                    processClasses(field, inventoryWindow, vocabularies);
                                }

                                alreadyInit = true;
                            } else {
                                result = method.invoke(object, args);
                            }
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);

                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.WButton", "setLabel", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Vocabulary[] vocabularies = new Vocabulary[]{translated.skills, translated.gui, translated.items};
                            String label = args[0].toString();
                            // Pattern for a craft label like: "kindling: 95%".
                            Pattern pattern01 = Pattern.compile("^([\\W\\w\\s]+): ([\\d\\%]+)$", Pattern.UNICODE_CHARACTER_CLASS);
                            Pattern pattern02 = Pattern.compile("^([+*\\w\\s]+)(: [\\d]*%)( \\(dif\\:[\\d]*\\))([\\W\\w]*)$", Pattern.UNICODE_CHARACTER_CLASS);
                            Matcher matcher01 = pattern01.matcher(label);
                            Matcher matcher02 = pattern02.matcher(label);
                            if (matcher01.matches()) {
                                label = getTranslate(matcher01.group(1), vocabularies) + ": " + matcher01.group(2);
                            } else if (matcher02.matches()) {
                                label = getTranslate(matcher02.group(1), vocabularies) + matcher02.group(2) + matcher02.group(3) + matcher02.group(4);
                            } else {
                                // Bad solution.
                                if(label.equals(getReverseTranslate(label,vocabularies, false))) {
                                    label = getTranslate(label, vocabularies);
                                }
                            }
                            label = capitalizeFirstLetter(label);
                            args[0] = label;
                            result = method.invoke(object, args);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });


        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.WButton", "setHoverString", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Vocabulary[] vocabularies = new Vocabulary[]{translated.gui};
                            String input = (String) args[0];
                            String output = "";
                            // regexp for hovers like "Stop or main menu  [Escape]"
                            Pattern pattern = Pattern.compile("^([\\s\\w]+\\w)(\\s*\\[\\s*\\w+\\])$");
                            Matcher matcher = pattern.matcher(input);
                            if (matcher.matches()) {
                                output = getTranslate(matcher.group(1), vocabularies) + matcher.group(2);
                                output = capitalizeFirstLetter(output);
                            } else {
                                output = getTranslate(input, vocabularies);
                            }
                            args[0] = output;
                            result = method.invoke(object, args);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });

        HookManager.getInstance().registerHook("com.wurmonline.client.renderer.gui.WCheckBox", "setLabel", null, new InvocationHandlerFactory() {
            @Override
            public InvocationHandler createInvocationHandler() {
                return new InvocationHandler() {
                    @Override
                    public Object invoke(Object object, Method method, Object[] args) throws Throwable {
                        Object result;
                        try {
                            Vocabulary[] vocabularies = new Vocabulary[]{translated.gui};
                            String label = args[0].toString();
                            label = getTranslate(label, vocabularies);
                            label += " ";
                            args[0] = label;
                            result = method.invoke(object, args);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                            result = method.invoke(object, args);
                        }
                        return result;
                    }
                };
            }
        });
    }

    private void processClasses(Field field, Object object, Vocabulary[] vocabularies) throws
            NoSuchFieldException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException,
            InvocationTargetException, InstantiationException {
        Class fieldClass = null;
        Field labelField = null;
        field.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        Object fieldInstance = field.get(object);

        try {
            if (!field.getType().isPrimitive()) {
                fieldClass = Class.forName(field.getType().getName());
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
        String fieldClassName = fieldClass != null ? fieldClass.getName() : "";
        switch (fieldClassName) {
            case "com.wurmonline.client.renderer.gui.WurmRadioButton":
                vocabularies = new Vocabulary[]{translated.gui};
                assert fieldClass != null;
                labelField = fieldClass.getDeclaredField("label");
                if (labelField != null && fieldInstance != null) {
                    String label = getPrivateField(fieldInstance, labelField);
                    // Bad hack for check a text was translated already. Alternative is to set hook for every class.
                    // We do it once, so we don't worry about performance.
                    if (label.equals(getReverseTranslate(label, vocabularies, false))) {
                        label = getTranslate(label, vocabularies);
                        label += " ";
                    }
                    setPrivateField(fieldInstance, labelField, label.toLowerCase());
                    // Recalculate a size for new label.
                    Field text = getRecursiveField(fieldClass, "text");
                    Object width = callMethod(fieldInstance, new Class[]{String.class}, text, "getWidth", new Object[]{label});
                    Object height = callMethod(fieldInstance, text, "getHeight", new Object[]{});
                    // Do like the origin code:
                    // >>> this.setSize(this.text.getWidth(label) + 16, this.text.getHeight() + 1);
                    Object[] args = new Object[]{(int) width + 16, (int) height + 1};
                    // Call setSize method.
                    callMethod(object, new Class[]{int.class, int.class}, field, "setSize", args);
                }
                break;

//            case "com.wurmonline.client.renderer.gui.WCheckBox":
//                vocabularies = new Vocabulary[]{translated.gui};
//                assert fieldClass != null;
//                labelField = fieldClass.getDeclaredField("label");
//                if (labelField != null && fieldInstance != null) {
//                    String label = getPrivateField(fieldInstance, labelField);
//                    if (label.equals(getReverseTranslate(label, vocabularies, false))) {
//                        label = getTranslate(label, vocabularies);
//                        label += " ";
//                    }
//                    // Call setLabel method.
//                    callMethod(object, field, "setLabel", new Object[]{label});
//                }
//                break;

            case "com.wurmonline.client.renderer.gui.WurmTextPanel":
                vocabularies = new Vocabulary[]{translated.gui};
                assert fieldClass != null;
                Field linesField = fieldClass.getDeclaredField("lines");
                List<?> lines = (List<?>) getPrivateField(fieldInstance, linesField);
                Field textFont = Translation.getRecursiveField(fieldClass, "text");
                for (Object line : lines) {
                    String text = (String) Translation.getFieldValue(line, "text");
                    text = getTranslate(text, vocabularies);
                    int width = (int) Translation.callMethod(fieldInstance, new Class[]{String.class}, textFont, "getWidth", new Object[]{text});
                    Translation.setFieldValue(line, "text", text);
                    Translation.setFieldValue(line, "width", width);
                    Translation.callMethod(object, field, "resize", new Object[]{});
                }
                break;

            case "com.wurmonline.client.renderer.gui.WurmInputField":
                vocabularies = new Vocabulary[]{translated.gui};
                assert fieldClass != null;
                Field inputField = fieldClass.getDeclaredField("input");
                if (inputField != null && fieldInstance != null) {
                    String title = getPrivateField(fieldInstance, inputField);
                    title = getTranslate(title, vocabularies);
                    setPrivateField(fieldInstance, inputField, title);
                }
                break;

        }
    }

    private void processSuperClasses(Field field, Object object, Vocabulary[] vocabularies) throws NoSuchFieldException, IllegalAccessException {
        Class fieldSuperClass = null;
        try {
            if (!field.getType().isPrimitive()) {
                fieldSuperClass = Class.forName(field.getType().getName()).getSuperclass();
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
        String fieldSuperClassName = fieldSuperClass != null ? fieldSuperClass.getName() : "";
        switch (fieldSuperClassName) {
            case "com.wurmonline.client.renderer.gui.WWindow":
                field.setAccessible(true);
                assert fieldSuperClass != null;
                Field titleField = fieldSuperClass.getDeclaredField("title");
                Object fieldInstance = field.get(object);
                if (titleField != null && fieldInstance != null) {
                    String title = getPrivateField(fieldInstance, titleField);
                    // Bad hack for check a text was translated already. Alternative is to set hook for every class.
                    // We do it once, so we don't worry about performance.
                    if (title.equals(getReverseTranslate(title, vocabularies, false))) {
                        title = getTranslate(title, vocabularies);
                    }
                    setPrivateField(fieldInstance, titleField, title);
                }
                break;
        }
    }
}
