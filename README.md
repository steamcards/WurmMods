Project support:
https://yasobe.ru/na/rus_wurm
--------------------------------------------------------------------------------------------------------
12.06.2019

Added Russian translation by Leo.

change `language=RU_Leo` in `mods\translation.properties` to activate it.

--------------------------------------------------------------------------------------------------------
Release 0.9-beta added. 

A translation of cooking, skills, paper doll added.

A receipes translation (RU) by Andy Dufresne.

--------------------------------------------------------------------------------------------------------

Release 0.8-beta added. 

A partially translation of interface added.

RU translation by Andy Dufresne.

Reworked a save mechanics of untranslated words. No more doubles.

--------------------------------------------------------------------------------------------------------
Wurm Unlimited client translation (items, craft, mobs).

DECLAIMER: It's beta version! Don't judge strictly a code quality.

ATTENTION! Depend on ago's Client mod loader: https://github.com/ago1024/WurmClientModLauncher/releases/

How to add another language?

1) Change in translation.properties file the  parameter 'language' to your language. 

For example: language=FR

2) Create a dir with same name in translation.

3) Copy files from 'translation/RU' to new directory 'translation/FR'.

4) Replace translate in xml files.

Dictionaries:

creatures.xml - translation of NPC and mobs

items.xml - translation of craft items, cooking, plants, animal parts etc

materials.xml - names of materials

prefixes.xml - item prefixes to decrease size of dictonaries

roots.xml - word roots for cyrillic languages. if your not is, copy data from 'materials.xml'

interface.xml - main menu, windows title, context menu

skills.xml - game skills

tiles.xml - terrain, caves